# ~/.bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return
	if [ -n "$DISPLAY" ];
	 then
		export BROWSER=librewolf;
		export EDITOR=geany;
	 else
		export EDITOR=nano
	fi

[[ -s "/usr/share/bash-completion/bash_completion" ]] && source /usr/share/bash-completion/bash_completion
[[ -s "/usr/share/fzf/key-bindings.bash" ]] && source /usr/share/fzf/key-bindings.bash
[[ -s "/usr/share/fzf/completion.bash" ]] && source /usr/share/fzf/completion.bash
[[ -s "/usr/share/git/completion/git-prompt.sh" ]] && source /usr/share/git/completion/git-prompt.sh

unset SSH_AGENT_PID
if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ]; then
	SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
	export SSH_AUTH_SOCK
fi
GPG_TTY=$(tty)
export GPG_TTY
gpg-connect-agent updatestartuptty /bye >/dev/null

export PATH="${PATH}:/$HOME/bin"

export XDG_CACHE_HOME=$HOME/.cache
export XDG_CONFIG_HOME=$HOME/.config
export XDG_DATA_HOME=$HOME/.local/share
export MAGICK_TEMPORARY_PATH="$HOME/.cache"
export MAGICK_MEMORY_LIMIT=4Gib
export MAGICK_MAP_LIMIT=4Gib
#export ALSA_CARD=USB
export VST_PATH=/usr/lib/vst:/usr/local/lib/vst:~/.vst
export LXVST_PATH=/usr/lib/lxvst:/usr/local/lib/lxvst:~/.lxvst
export LADSPA_PATH=/usr/lib/ladspa:/usr/local/lib/ladspa:~/.ladspa
export LV2_PATH=/usr/lib/lv2:/usr/local/lib/lv2:~/.lv2
export DSSI_PATH=/usr/lib/dssi:/usr/local/lib/dssi:~/.dssi
export HISTFILESIZE=20000
export HISTSIZE=20000
export HISTIGNORE="&:ls:[bf]g:exit:history:man"
export HISTCONTROL=ignoreboth:erasedups

export LESS_TERMCAP_mb=$'\e[01;31m'
export LESS_TERMCAP_md=$'\e[01;38;5;74m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_so=$'\e[0;30;47m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[04;38;5;146m'
export PAGER=less
export MANPAGER="less -R --use-color -Dd+r -Du+b"

#COLOR
GRC_ALIASES=true
[[ -s "/etc/profile.d/grc.sh" ]] && source /etc/profile.d/grc.sh

alias jonff='$HOME/scripts/jack_on-off.sh'
alias j='$HOME/bin/artix-pipewire-launcher restart &'
alias yu='$HOME/scripts/ytdlp.bash best'
alias urldecode='python -c "import sys, urllib, urllib.parse; print(urllib.parse.unquote_plus(sys.argv[1]))"'
alias hQ='history|grep -i'
alias rm='rm -I'
#alias yQ='yay -Ql | grep -i'
alias ls='ls -lhFa --color=auto --group-directories-first'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias df='df -h'
alias du='du -h'
alias gcp='rsync -pogrh --info=progress2'

#function for searching in history every call of executable file from given package
hwo()
{
local Yellow='\033[0;33m'
local BBlack='\033[1;30m'
local Yellow_BG='\033[43m'
local ClearColor='\033[0m'
for f in $(paru -Ql "$1" | grep bin | awk '{print $2}' | awk -F '/' '{print $4}');
	do LIST+=("$f");
	hQ -w "$f";
	done;
echo -e "\n${Yellow}------------------${ClearColor}\n";
echo -e "${Yellow_BG}${BBlack}${LIST[*]}${ClearColor}";
unset LIST
}
_pacman_list()
{
local cur=${COMP_WORDS[COMP_CWORD]}
pkg_list=$(pacman -Q 2>/dev/null | cut -d' ' -f1)
COMPREPLY=( $(compgen -W "${pkg_list}" -- "$cur") )
}
complete -F _pacman_list hwo

pagrep()
{
grep --color=auto -B 3 -A 3 -i "$1" /var/log/pacman.log
}

alias dmesg1='tail -f -n 100 /var/log/kernel.log | bat --paging=never -l log'

#keys for terminal
bind '";5D":backward-word'                     # ctrl+left
bind '";5C":forward-word'                      # ctrl+right
bind '"\C-p": "\C-e\C-u xsel <<"EOF"\n\C-y\nEOF\n\C-y"'   #clipboard CTRL-P to copy string, SHIFT-INSERT to paste

# whoneeds replacement via pactree
whoneeds() {
grep Description <(LANG=EN pacman -Qi "$1")
echo "─────────────────────────────────" 
comm -12 <(pactree -lru "$1"|sort) <(pacman -Qqe|sort) | grep -v "$1"
}

complete -F _pacman_list whoneeds
alias whon=whoneeds
complete -F _pacman_list whon

#repacman bash completion
#_repacman() {
#    local cur prev packages opts
#    _get_comp_words_by_ref cur prev
#    COMPREPLY=()
#    packages=$(pacman -Qq)
#    opts=$(_parse_help $1 -h)
#    if [[ $COMP_CWORD -eq 1 ]]; then
#        COMPREPLY=( $( compgen -W "$opts" -- "$cur" ) )
#        COMPREPLY+=( $( compgen -W "$packages" -- "$cur" ) )
#    else
#        if [[ "$prev" = '-h' ]]; then
#            return
#        fi
#        COMPREPLY=( $( compgen -W "$packages" -- "$cur" ) )
#    fi
#} &&
#complete -F _repacman repacman

# append to the history file, don't overwrite it
shopt -s histappend
# don't need to 'cd' before path for change directory
shopt -s autocd
# check the window size after each command and, if necessary, update the values of LINES and COLUMNS.
shopt -s checkwinsize
# correct minor errors in the spelling of a directory component in a cd command
shopt -s cdspell
# save all lines of a multiple-line command in the same history entry (allows easy re-editing of multi-line commands)
shopt -s cmdhist



if [ -f ~/.hostname ]; then
    LOCAL_HOSTNAME=$(cat ~/.hostname)
else
    LOCAL_HOSTNAME=$HOSTNAME
fi

# Handy Extract Program
#function extract()
#{
#    if [ -f $1 ] ; then
#        case $1 in
#            *.tar.bz2)   tar xvjf $1     ;;
#            *.tar.gz)    tar xvzf $1     ;;
#            *.bz2)       bunzip2 $1      ;;
#            *.rar)       unrar x $1      ;;
#            *.gz)        gunzip $1       ;;
#            *.tar)       tar xvf $1      ;;
#            *.tbz2)      tar xvjf $1     ;;
#            *.tgz)       tar xvzf $1     ;;
#            *.zip)       unzip $1        ;;
#            *.Z)         uncompress $1   ;;
#            *.7z)        7z x $1         ;;
#            *)           echo "'$1' cannot be extracted via >extract<" ;;
#        esac
#    else
#        echo "'$1' is not a valid file!"
#    fi
#}
# Запаковать архив
# example: pk tar file
#function pk()
#{
#	if [ $1 ] ; then
#		case $1 in
#			tbz)       tar cjvf $2.tar.bz2 $2      ;;
#			tgz)       tar czvf $2.tar.gz  $2       ;;
#			tar)      tar cpvf $2.tar  $2       ;;
#			bz2)    bzip $2 ;;
#			gz)        gzip -c -9 -n $2 > $2.gz ;;
#			zip)       zip -r $2.zip $2   ;;
#			7z)        7z a $2.7z $2    ;;
#			*)         echo "'$1' cannot be packed via pk()" ;;
#		esac
#	else
#		echo "'$1' is not a valid file"
#	fi
#}   
function prompt_command
{
	local PWDNAME=$PWD
	local GIT_PS1_SHOWCOLORHINTS=1           # Make pretty colours inside $PS1
	local GIT_PS1_SHOWDIRTYSTATE=1           # '*'=unstaged, '+'=staged
	local GIT_PS1_SHOWSTASHSTATE=1           # '$'=stashed
	local GIT_PS1_SHOWUNTRACKEDFILES=1       # '%'=untracked
	local GIT_PS1_HIDE_IF_PWD_IGNORED=1      # don't show git status if we're inside gitignored dir
	local GIT_PS1_SHOWUPSTREAM="auto"     # 'u='=no difference, 'u+1'=ahead by 1 commit
#	local GIT_PS1_STATESEPARATOR=''          # No space between branch and index status
	local GIT_PS1_DESCRIBE_STYLE="describe"  # Detached HEAD style:
                                             # describe      relative to older annotated tag (v1.6.3.1-13-gdd42c2f)
                                             # contains      relative to newer annotated tag (v1.6.3.2~35)
                                             # branch        relative to newer tag or branch (master~4)
                                             # default       exactly eatching tag
	local GITPS1; GITPS1=$(__git_ps1 "(%s) ")
	local GITPS1NUM; GITPS1NUM=$(__git_ps1 "(%s) " | sed -r 's~\x01?(\x1B\(B)?\x1B\[([0-9;]*)?[JKmsu]\x02?~~g')
	local color_green="\[\e[01;38;05;110m\]"
	local color_red="\[\e[01;38;05;196m\]"
	local color_yellow='\033[0;33m'
	local color_off="\[\e[m\]"
	local color_host="\[\e[38;05;242m\]"
	local color_wd="\[\e[0;34m\]"

# beautify working directory name
	if [[ "${HOME}" == "${PWD}" ]]; then
		PWDNAME="~"
	elif [[ "${HOME}" == "${PWD:0:${#HOME}}" ]]; then
	#  ${var:pos:len} Подстанавливается значение переменной var, начиная с позиции pos, не более len символов
		PWDNAME="~${PWD:${#HOME}}"
	fi

# set user color
		case $(id -u) in
			0) color_user=$color_red ;;
			*) color_user=$color_green ;;
		esac

# calculate prompt length
	local PS1_length=$((${#USER}+${#LOCAL_HOSTNAME}+${#PWDNAME}+${#GITPS1NUM}+14))
	local FILL=

# if length is greater, than terminal width
	if [[ $PS1_length -gt $COLUMNS ]]; then
# strip working directory name
		PWDNAME="...${PWDNAME:$((PS1_length-COLUMNS+3))}"
	else
# else calculate fillsize
		local fillsize=$((COLUMNS-PS1_length))
		FILL=$color_user
		while [[ $fillsize -gt 0 ]]; do FILL="${FILL}─"; fillsize=$((fillsize-1)); done
		FILL="${FILL}${color_off}"
	fi

# set new color prompt
	PS1="\n${color_user}┌─@${color_off}${color_host}${LOCAL_HOSTNAME}${color_off} :: ${color_user}${USER}${color_off}${color_wd} [ ${PWDNAME} ] ${color_off}${GITPS1}${FILL}\n${color_user}└─➤ ${color_off}"
	# set cursor color
	if [ "$TERM" != "linux" ];	then
		echo -ne "\033]12;LightBlue\007"
	fi

# set title
# echo -ne "\033]0;${USER}@${LOCAL_HOSTNAME}:${PWDNAME}"; echo -ne "\007"

history -a
}


PROMPT_COMMAND=prompt_command
# PS1='\n\[\e[38;05;146;48;05;68m\]@ \H \[\e[01;38;05;152;48;05;68m\]\u\[\e[0;34m\]: [\W] \n\[\e[1;32m\]\$\[\e[0m\] '
PS1='[\u@\h \W]\$ '

complete -cf sudo

PATH="$HOME/perl5/bin${PATH:+:${PATH}}"; export PATH;
PERL5LIB="$HOME/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="$HOME/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
PERL_MM_OPT="INSTALL_BASE=$HOME/perl5"; export PERL_MM_OPT;
