--[[
Conky, a system monitor, based on torsmo

Any original torsmo code is licensed under the BSD license

All code written since the fork of torsmo is licensed under the GPL

Please see COPYING for details 
 
Copyright (c) 2004, Hannu Saransaari and Lauri Hakkarainen
Copyright (c) 2005-2012 Brenden Matthews, Philip Kovacs, et. al. (see AUTHORS)
All rights reserved.

This program is free software: you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
]]

conky.config = {
    alignment = 'top_left',
    double_buffer = true,
    background = false,
    border_width = 0,
    cpu_avg_samples = 2,
    default_color = 'white',
    default_outline_color = '2d2d2d',
    default_shade_color = '3d3d3d',
    draw_borders = false,
    draw_graph_borders = false,
    draw_outline = false,
    draw_shades = false,
    use_xft = true,
    font = 'PT Sans:size=8',
    gap_x = 0,
    gap_y = 0,
    minimum_height = 1080,
    minimum_width = 380,
    net_avg_samples = 2,
    no_buffers = true,
    out_to_console = false,
    out_to_stderr = false,
    extra_newline = false,
    own_window = true,
    own_window_transparent = false,
    own_window_argb_visual = true,
    own_window_argb_value = 0,
    own_window_class = 'Conky',
    own_window_type = 'desktop',
    stippled_borders = 0,
    update_interval = 1.0,
    uppercase = false,
    use_spacer = 'none',
    show_graph_scale = false,
    show_graph_range = false,
    override_utf8_locale = true,
    imlib_cache_size = 0,
    pad_percents = 2,
    color0 = '11aaff',
    color1 = 'ffffff',
    color2 = '777777',
    color3 = '99BBFF',
    color4 = '9090FF',
    color5 = '858585',
    color6 = 'DDDDDD',
    color8 = '80A0FF',
    color9 = 'FF0000',
}

conky.text = [[
























${GOTO 16}${font Pt Sans:bold:size=12}${color0}Disks${font}${color}

${GOTO 26}RootSSD:${GOTO 87}${fs_bar 10,115 /} ${fs_free /} of ${fs_size /} free${GOTO 87} ${color 1010aa}${voffset -1}${execi 5 sudo hddtemp -n /dev/disk/by-id/ata-Patriot_P200_256GB_AA000000000000001876}ºC${voffset 1}${color}${GOTO 137}${diskiograph /dev/disk/by-id/ata-Patriot_P200_256GB_AA000000000000001876 10,65 99BBFF 11aaff}
${voffset -4}${GOTO 26}taw3:${GOTO 87}${fs_bar 10,115 /media/taw3} ${fs_free /media/taw3} of ${fs_size /media/taw3} free${GOTO 87} ${color 1010aa}${voffset -1}${execi 5 sudo hddtemp -n /dev/disk/by-uuid/983bf457-8949-4f81-99ee-355a61d865f4}ºC${voffset 1}${color}${GOTO 137}${diskiograph /dev/disk/by-uuid/983bf457-8949-4f81-99ee-355a61d865f4 10,65 99BBFF 11aaff}

${execpi 1 $HOME/scripts/usb_conky.sh}

${GOTO 16}${font Pt Sans:bold:size=12}${color0}Network${font}${color}

${GOTO 26}${font PizzaDude Bullets:size=14}M${font}${voffset -2} Up:${voffset 2}${GOTO 87}${upspeedgraph eth0 10,65 ffffff ffffff}  ${voffset -1}${upspeed eth0}
${voffset 2}${GOTO 26}${font PizzaDude Bullets:size=14}U${font}${voffset -2} Dwn:${voffset 2}${GOTO 87}${downspeedgraph eth0 10,65 ffffff ffffff}  ${voffset -1}${downspeed eth0}

${GOTO 39}${color3}IP:${color}${GOTO 63}${addr eth0} / ${execi 30 cat /tmp/ip4_nvcmf}
#${voffset -4}${GOTO 39}${color3}IP6:${color}${GOTO 63}${execi 1200 ip -6 -o addr show eth0 | grep global | grep -v 0sec | awk -F' ' '{print $4}' |awk -F'/' '{print $1}'}
${voffset -4}${GOTO 39}${color3}IP6:${color}${GOTO 63}${execi 1200 python -c "import netifaces; print(netifaces.ifaddresses('eth0')[netifaces.AF_INET6][0]['addr'])"}

${GOTO 16}${font Pt Sans:bold:size=12}${color0}System${font}${color}

${GOTO 26}${color3}Kernel: ${color}${kernel}   ${color3}uptime: ${color}${uptime_short}
${voffset -4}${GOTO 26}${color3}Updates: ${color}${execi 3600 echo $(checkupdates | wc -l)}

#${GOTO 26}${color3}NVidia GPU / Mem:${color}    ${nvidia gpufreq} / ${nvidia memfreq} ${color3}Tº:${color} ${nvidia temp}ºC
#${execi 5 nvidia-smi -q -d TEMPERATURE | grep "GPU Current Temp"}
${GOTO 26}${color3}CPU:${color}${GOTO 87}${cpubar cpu0 10,115} ${cpu cpu0}%${GOTO 250}${color3}@${color}${freq_g} GHz${GOTO 87}${cpugraph cpu0 10,115 99BBFF 11aaff}
#${GOTO 26}${color3}CPU1:${color}${GOTO 87}${cpubar cpu1 10,115} ${cpu cpu1}%${GOTO 250}${color3}@${color}${freq_g 1} GHz${GOTO 87}${cpugraph cpu1 10,115 555555 11aaff}${GOTO 320}${color3}Tº:${color} ${execi 4 sensors | grep -A 0 'temp2' | cut -c16-17} ºC
#${GOTO 26}${color3}CPU2:${color}${GOTO 87}${cpubar cpu2 10,115} ${cpu cpu2}%${GOTO 250}${color3}@${color}${freq_g 2} GHz${GOTO 87}${cpugraph cpu2 10,115 555555 11aaff}
#${GOTO 26}${color3}CPU3:${color}${GOTO 87}${cpubar cpu3 10,115} ${cpu cpu3}%${GOTO 250}${color3}@${color}${freq_g 3} GHz${GOTO 87}${cpugraph cpu3 10,115 555555 11aaff}
#${GOTO 26}${color3}CPU4:${color}${GOTO 87}${cpubar cpu4 10,115} ${cpu cpu4}%${GOTO 250}${color3}@${color}${freq_g 4} GHz${GOTO 87}${cpugraph cpu4 10,115 555555 11aaff}
#${GOTO 320}${color3}Tº:${color} ${execi 4 sensors | grep 'Core1' | cut -c15-19}ºC
${voffset -4}${GOTO 26}${color3}RAM:${color}${GOTO 87}${if_match ${memperc} < 85}${membar 10,115} ${memperc}%${GOTO 250}${memeasyfree} of ${memmax} free${else}${color FF0000}${membar 10,115} ${memperc}%${GOTO 250}${memeasyfree}${color} of ${memmax} free${endif}
${voffset -4}${GOTO 26}${color3}/tmp:${color}${GOTO 87}${fs_bar 10,115 /tmp} ${fs_free /tmp} of ${fs_size /tmp} free
${voffset -4}${GOTO 26}${color3}ShM:${color}${GOTO 87}${fs_bar 10,115 /dev/shm} ${fs_free /dev/shm} of ${fs_size /dev/shm} free
${voffset -4}${GOTO 26}${color3}SWAP:${color}${GOTO 87}${swapbar 10,115} ${swapfree} of ${swapmax} free
#${voffset -4}${execpi 5 $HOME/scripts/swap_conky.sh}
${GOTO 26}${color3}CPU Fan:${color}${GOTO 87}${execi 4 sensors it8720-isa-0228 | grep -A 0 'fan1' | cut -c14-21} / ${execi 4 sensors it8720-isa-0228 | grep -A 0 'temp2' | cut -c16-17} ºC
#${GOTO 26}${color3}Sys Fan:${color}${GOTO 87}${execi 4 sensors it8720-isa-0228 | grep -A 0 'fan2' | cut -c15-21}
${voffset -4}${GOTO 26}${color3}GPU:${GOTO 87}${color}${execi 4 sensors amdgpu-pci-0100 2>/dev/null | grep -A 0 'edge' | cut -c16-17} ºC
#${GOTO 26}${color3}UPS: ${if_match "${execi 5 upsc nutdev1 ups.status}" == "OL BOOST"}${color F06900}Online boosted${color}${else}${color}${execi 5 upsc nutdev1 ups.status}${endif}
#${GOTO 33}${color3}Input voltage: ${color}${execi 1 upsc nutdev1 input.voltage}V
#${voffset -4}${GOTO 33}${color3}Charge: ${color}${execi 1 upsc nutdev1 battery.charge}%; ${color3}left:${color} ${execi 1 upsc nutdev1 battery.runtime} sec
#${voffset -4}${GOTO 33}${color3}Load: ${color}${execi 1 upsc nutdev1 ups.load}%
#${GOTO 26}${color3}JACK ${if_match "${execi 5 jack_control status >& /dev/null; echo $?}" == "0"}${color}Connected${else}${color9}Disconnected${color}${image $HOME/.config/conky/images/disconnected.png -p 7, 810}${endif}

#${GOTO 26}${top name 1}  ${top cpu 1}   ${top mem_res 1} 
#${GOTO 26}${top name 2}  ${top cpu 2}   ${top mem_res 2}
#${GOTO 26}${top name 3}  ${top cpu 3}   ${top mem_res 3} 

${if_match "${execi 3600 date +%a}" == "Пн"}${image $HOME/.config/conky/images/pinkie_pie_by_moritoakira.png -p 0, 880}${endif}
${if_match "${execi 3600 date +%a}" == "Вт"}${image $HOME/.config/conky/images/pinkie_pie_suspicious.png -p 70, 885}${endif}
${if_match "${execi 3600 date +%a}" == "Ср"}${image $HOME/.config/conky/images/pinkie_pie_breaks_the_4th_wall_again__by_berrycherryart.png -p 60, 875}${endif}
${if_match "${execi 3600 date +%a}" == "Чт"}${image $HOME/.config/conky/images/pinkie-pie_1.png -p 30, 880}${endif}
${if_match "${execi 3600 date +%a}" == "Пт"}${image $HOME/.config/conky/images/pinkie_pie_cheering.png -p 70, 870}${endif}
${if_match "${execi 3600 date +%a}" == "Сб"}${image $HOME/.config/conky/images/pinkie_pie_cute.png -p 30, 880}${endif}
${if_match "${execi 3600 date +%a}" == "Вс"}${image $HOME/.config/conky/images/pinkie-pie_2.png -p 30, 880}${endif}


]]
